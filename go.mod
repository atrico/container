module gitlab.com/atrico/container/v2

go 1.24

require (
	github.com/segmentio/ksuid v1.0.4
	gitlab.com/atrico/testing/v2 v2.5.3
)

require golang.org/x/exp v0.0.0-20250218142911-aa4b98e5adaa // indirect
