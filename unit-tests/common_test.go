package unit_tests

import (
	"fmt"
	"gitlab.com/atrico/container/v2"
	"math"
	"strings"
)

// ----------------------------------------------------------------------------------------------------------------------------
// Named
// ----------------------------------------------------------------------------------------------------------------------------

type namedRegisterTestCase struct {
	name          string
	configurators [][]container.Configurator
}

func namedRegisterTestCases(count int) (testCases []namedRegisterTestCase) {
	perms := int(math.Pow(2, float64(count)))
	for i := 0; i < perms; i++ {
		var tc namedRegisterTestCase
		n := make([]string, 0, count)
		for r := 0; r < count; r++ {
			if (i & (1 << r)) > 0 {
				n = append(n, "S")
				tc.configurators = append(tc.configurators, []container.Configurator{container.Singleton})
			} else {
				n = append(n, "T")
				tc.configurators = append(tc.configurators, []container.Configurator{container.Transient})
			}
		}
		tc.name = strings.Join(n, ",")
		testCases = append(testCases, tc)
	}
	return
}

// ----------------------------------------------------------------------------------------------------------------------------
// Map and Slice
// ----------------------------------------------------------------------------------------------------------------------------

type mapSliceRegisterTestCase[T any] struct {
	namedRegisterTestCase
	resolver func(c container.ContainerBuilder) T
}

func mapSliceRegisterTestCases[T any](count int) (testCases []mapSliceRegisterTestCase[T]) {
	for _, tc := range namedRegisterTestCases(count) {
		oldName := tc.name
		tc.name = fmt.Sprintf("Direct %s", oldName)
		testCases = append(testCases, mapSliceRegisterTestCase[T]{tc, resolveDirect[T]})
		tc.name = fmt.Sprintf("Param %s", oldName)
		testCases = append(testCases, mapSliceRegisterTestCase[T]{tc, resolveParam[T]})
	}
	return
}

func resolveDirect[T any](c container.ContainerBuilder) T {
	var result T
	c.Resolve(&result)
	return result
}

type Wrapper[T any] struct {
	content T
}

func resolveParam[T any](c container.ContainerBuilder) T {
	c.Register(func(content T) Wrapper[T] { return Wrapper[T]{content} })
	var wrapper Wrapper[T]
	c.Resolve(&wrapper)
	return wrapper.content
}

// ----------------------------------------------------------------------------------------------------------------------------
// Helper types
// ----------------------------------------------------------------------------------------------------------------------------
type TheType string
