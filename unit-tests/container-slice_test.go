package unit_tests

import (
	"gitlab.com/atrico/container/v2"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"testing"
)

func Test_ResolveSliceNamed(t *testing.T) {
	for _, testCase := range mapSliceRegisterTestCases[[]TheType](4) {
		t.Run(testCase.name, func(t *testing.T) {
			func(testCase mapSliceRegisterTestCase[[]TheType]) {
				// Arrange
				const name1 = "name1"
				const name2 = "name2"
				const name3 = "name3"
				const name4 = "name4"
				c := container.NewContainer().
					Register(func() TheType { return TheType(name1) }, append(testCase.configurators[0], container.Name(name1))...).
					Register(func() TheType { return TheType(name2) }, append(testCase.configurators[1], container.Name(name2))...).
					Register(func() TheType { return TheType(name3) }, append(testCase.configurators[2], container.Name(name3))...).
					Register(func() TheType { return TheType(name4) }, append(testCase.configurators[3], container.Name(name4))...)

				// Act
				result := testCase.resolver(c)

				// Assert
				expected := []TheType{
					TheType(name1),
					TheType(name2),
					TheType(name3),
					TheType(name4),
				}
				assert.That(t, result, is.EquivalentTo(expected), "slice")
			}(testCase)
		})
	}
}

func Test_ResolveSliceNamedPlusDefault(t *testing.T) {
	for _, testCase := range mapSliceRegisterTestCases[[]TheType](4) {
		t.Run(testCase.name, func(t *testing.T) {
			func(testCase mapSliceRegisterTestCase[[]TheType]) {
				// Arrange
				const name1 = "name1"
				const name2 = "name2"
				const name3 = "name3"
				const name4 = ""
				c := container.NewContainer().
					Register(func() TheType { return TheType(name1) }, append(testCase.configurators[0], container.Name(name1))...).
					Register(func() TheType { return TheType(name2) }, append(testCase.configurators[1], container.Name(name2))...).
					Register(func() TheType { return TheType(name3) }, append(testCase.configurators[2], container.Name(name3))...).
					Register(func() TheType { return TheType(name4) }, append(testCase.configurators[3], container.Name(name4))...)

				// Act
				result := testCase.resolver(c)

				// Assert
				expected := []TheType{
					TheType(name1),
					TheType(name2),
					TheType(name3),
					TheType(name4),
				}
				assert.That(t, result, is.EquivalentTo(expected), "slice")
			}(testCase)
		})
	}
}

func Test_ResolveSliceOverride(t *testing.T) {
	for _, testCase := range mapSliceRegisterTestCases[[]TheType](5) {
		t.Run(testCase.name, func(t *testing.T) {
			func(testCase mapSliceRegisterTestCase[[]TheType]) {
				expected := []TheType{
					TheType("one"),
					TheType("two"),
				}
				// Arrange
				const name1 = "name1"
				const name2 = "name2"
				const name3 = "name3"
				const name4 = "name4"
				c := container.NewContainer().
					Register(func() TheType { return TheType(name1) }, append(testCase.configurators[0], container.Name(name1))...).
					Register(func() TheType { return TheType(name2) }, append(testCase.configurators[1], container.Name(name2))...).
					Register(func() TheType { return TheType(name3) }, append(testCase.configurators[2], container.Name(name3))...).
					Register(func() TheType { return TheType(name4) }, append(testCase.configurators[3], container.Name(name4))...).
					Register(func() []TheType { return expected }, testCase.configurators[4]...)

				// Act
				var result []TheType
				c.Resolve(&result)

				// Assert
				assert.That(t, result, is.EquivalentTo(expected), "slice")
			}(testCase)
		})
	}
}

func Test_ResolveSliceNone(t *testing.T) {
	for _, testCase := range mapSliceRegisterTestCases[[]TheType](5) {
		t.Run(testCase.name, func(t *testing.T) {
			func(testCase mapSliceRegisterTestCase[[]TheType]) {
				// Arrange
				c := container.NewContainer()

				// Act
				result := testCase.resolver(c)

				// Assert
				assert.That(t, result, is.EquivalentTo([]TheType{}), "slice")
			}(testCase)
		})
	}
}

func Test_ResolveSliceDefaultOnly(t *testing.T) {
	for _, testCase := range mapSliceRegisterTestCases[[]TheType](5) {
		t.Run(testCase.name, func(t *testing.T) {
			func(testCase mapSliceRegisterTestCase[[]TheType]) {
				// Arrange
				const value = "value"
				c := container.NewContainer().
					Register(func() TheType { return TheType(value) })

				// Act
				result := testCase.resolver(c)

				// Assert
				assert.That(t, result, is.EquivalentTo([]TheType{value}), "slice")
			}(testCase)
		})
	}
}

func Test_ResolveSliceUnNamed(t *testing.T) {
	for _, testCase := range mapSliceRegisterTestCases[[]TheType](4) {
		t.Run(testCase.name, func(t *testing.T) {
			func(testCase mapSliceRegisterTestCase[[]TheType]) {
				// Arrange
				const name1 = "name1"
				const name2 = "name2"
				const name3 = "name3"
				const name4 = "name4"
				c := container.NewContainer().
					Register(func() TheType { return TheType(name1) }, append(testCase.configurators[0], container.Duplicate)...).
					Register(func() TheType { return TheType(name2) }, append(testCase.configurators[1], container.Duplicate)...).
					Register(func() TheType { return TheType(name3) }, append(testCase.configurators[2], container.Duplicate)...).
					Register(func() TheType { return TheType(name4) }, append(testCase.configurators[3], container.Duplicate)...)

				// Act
				result := testCase.resolver(c)

				// Assert
				expected := []TheType{
					TheType(name1),
					TheType(name2),
					TheType(name3),
					TheType(name4),
				}
				assert.That(t, result, is.EquivalentTo(expected), "slice")
			}(testCase)
		})
	}
}

func Test_ResolveSliceUnNamedPlusDefault(t *testing.T) {
	for _, testCase := range mapSliceRegisterTestCases[[]TheType](4) {
		t.Run(testCase.name, func(t *testing.T) {
			func(testCase mapSliceRegisterTestCase[[]TheType]) {
				// Arrange
				const name1 = "name1"
				const name2 = "name2"
				const name3 = "name3"
				const name4 = ""
				c := container.NewContainer().
					Register(func() TheType { return TheType(name1) }, append(testCase.configurators[0], container.Duplicate)...).
					Register(func() TheType { return TheType(name2) }, append(testCase.configurators[1], container.Duplicate)...).
					Register(func() TheType { return TheType(name3) }, append(testCase.configurators[2], container.Duplicate)...).
					Register(func() TheType { return TheType(name4) }, append(testCase.configurators[3], container.Duplicate)...)

				// Act
				result := testCase.resolver(c)

				// Assert
				expected := []TheType{
					TheType(name1),
					TheType(name2),
					TheType(name3),
					TheType(name4),
				}
				assert.That(t, result, is.EquivalentTo(expected), "slice")
			}(testCase)
		})
	}
}
