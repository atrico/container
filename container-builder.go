// Package container provides an IoC container for Go projects.
// It provides simple, fluent and easy-to-use interface to make dependency injection in GoLang easier.
package container

import (
	"github.com/segmentio/ksuid"
	"reflect"
)

// ContainerBuilder interface for registering abstractions
type ContainerBuilder interface {
	Container

	// Register factory function
	// It takes a resolver function which returns the concrete and its return type matches the abstraction (interface).
	// Function is called on Resolve
	// The resolver function can have arguments of abstraction that are bound in Container.
	Register(resolver any, configurators ...Configurator) ContainerBuilder

	// Clear will empty the container and remove all the bindings.
	Clear()
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------

func (c *container) Register(resolver any, configurators ...Configurator) ContainerBuilder {
	config := getConfig(configurators)
	c.bind(resolver, config)
	return c
}

func (c *container) Clear() {
	*c = map[reflect.Type]map[string]binding{}
}

func (c *container) Build() Container {
	return c
}

// bind will map an abstraction to a concrete.
func (c *container) bind(resolver any, config Config) {
	resolverTypeOf := reflect.TypeOf(resolver)
	if resolverTypeOf.Kind() != reflect.Func {
		panic("the resolver must be a function")
	}
	var bind binding
	var anonymous bool
	if config.duplicate && config.name == "" {
		anonymous = true
		config.name = ksuid.New().String()
	}
	if config.transient {
		bind = &bindingTransient{bindingCommon{resolver, anonymous}}
	} else {
		bind = &bindingSingleton{bindingCommon{resolver, anonymous}, nil}
	}
	for i := 0; i < resolverTypeOf.NumOut(); i++ {
		typ := resolverTypeOf.Out(i)
		if _, ok := (*c)[typ]; !ok {
			(*c)[typ] = make(map[string]binding, 1)
		}
		(*c)[typ][config.name] = bind
		// Is default already set?
		_, defaultSet := (*c)[typ][""]
		// Is this explicitly set?
		defaultExplicit := defaultSet && !(*c)[typ][""].isDefault()
		// Add as default if not already set or not explicit and this is marked as default
		if !defaultSet || (config.isDefault && !defaultExplicit) {
			(*c)[typ][""] = &bindingDefault{bind, config.name}
		}
	}
}

// ----------------------------------------------------------------------------------------------------------------------------
// Binding
// ----------------------------------------------------------------------------------------------------------------------------

type binding interface {
	resolve(c *container, name string) any
	isDefault() bool
	isAnonymous() bool
	status() BindInfo
}

type bindingCommon struct {
	resolver  any // resolver function
	anonymous bool
}

func (b *bindingCommon) resolve(c *container, name string) any {
	return c.invoke(name, b.resolver)
}

func (b *bindingCommon) isDefault() bool {
	return false
}

func (b *bindingCommon) isAnonymous() bool {
	return b.anonymous
}

// Singleton binding - only created once
type bindingSingleton struct {
	bindingCommon
	instance any // instance stored for singleton bindings (on first resolve)
}

func (b *bindingSingleton) resolve(c *container, name string) any {
	// Create instance if not already resolved
	if b.instance == nil {
		b.instance = c.invoke(name, b.resolver)
	}
	return b.instance
}

func (b *bindingSingleton) status() BindInfo {
	return BindInfo{Anonymous: b.isAnonymous()}
}

// Transient binding, new instance created each time it's resolved
type bindingTransient struct {
	bindingCommon
}

func (b *bindingTransient) status() BindInfo {
	return BindInfo{Transient: true, Anonymous: b.isAnonymous()}
}

// Default binding, references other binding
type bindingDefault struct {
	bind binding
	name string
}

func (b *bindingDefault) resolve(c *container, name string) any {
	return b.bind.resolve(c, name)
}

func (b *bindingDefault) isDefault() bool {
	return true
}

func (b *bindingDefault) isAnonymous() bool {
	return b.isAnonymous()
}

func (b *bindingDefault) status() BindInfo {
	return BindInfo{DefaultTo: b.name}
}
