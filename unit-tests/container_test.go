package unit_tests

import (
	"gitlab.com/atrico/container/v2"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"strings"
	"testing"
)

func Test_ResolveContainer(t *testing.T) {
	// Arrange
	c := container.NewContainer().
		Register(func(c container.Container) TheType { return TheType("") })

	// Act
	var result TheType
	c.Resolve(&result)

	// Assert
	assert.That[any](t, result, is.NotNil, "resolved")
}

func Test_Status(t *testing.T) {
	// Arrange
	c := container.NewContainer().
		Register(func() TheType2 { return TheType2("default2") }, container.Transient).
		Register(func() TheType { return TheType("one") }, container.Name("one")).
		Register(func() TheType { return TheType("two") }, container.Name("two")).
		Register(func() TheType { return TheType("dupl") }, container.Duplicate)

	// Act
	status := c.Status()
	t.Log("\n" + status.String())

	// Assert
	expected := strings.Join([]string{
		"container.Container:",
		"unit_tests.TheType:",
		"  - 'one': (default)",
		"  - 'two'",
		"  - DUPLICATE",
		"unit_tests.TheType2: (transient)",
	}, "\n")
	assert.That(t, status.String(), is.EqualTo(expected), "string")
}

func Test_StatusFilter(t *testing.T) {
	// Arrange
	c := container.NewContainer().
		Register(func() TheType2 { return TheType2("default2") }, container.Transient).
		Register(func() TheType { return TheType("one") }, container.Name("one")).
		Register(func() TheType { return TheType("two") }, container.Name("two")).
		Register(func() TheType { return TheType("dupl") }, container.Duplicate)

	// Act
	status := c.StatusWithFilter(`unit_tests\.`)
	t.Log("\n" + status.String())

	// Assert
	expected := strings.Join([]string{
		"unit_tests.TheType:",
		"  - 'one': (default)",
		"  - 'two'",
		"  - DUPLICATE",
		"unit_tests.TheType2: (transient)",
	}, "\n")
	assert.That(t, status.String(), is.EqualTo(expected), "string")
}
