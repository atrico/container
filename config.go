package container

type Config struct {
	transient bool
	name      string
	duplicate bool
	isDefault bool
}

func defaultConfig() Config {
	return Config{
		transient: false,
		name:      "",
	}
}

type Configurator func(config *Config)

func getConfig(configurators []Configurator) (config Config) {
	config = defaultConfig()
	for _, cfg := range configurators {
		cfg(&config)
	}
	return
}

// ----------------------------------------------------------------------------------------------------------------------------
// Transient (or Singleton)
// ----------------------------------------------------------------------------------------------------------------------------

var Transient Configurator = func(config *Config) {
	config.transient = true
}

var Singleton Configurator = func(config *Config) {
	config.transient = false
}

// ----------------------------------------------------------------------------------------------------------------------------
// Named
// ----------------------------------------------------------------------------------------------------------------------------

func Name(name string) Configurator {
	return func(config *Config) {
		config.name = name
	}
}

var NoName Configurator = func(config *Config) {
	config.name = ""
}

// ----------------------------------------------------------------------------------------------------------------------------
// Default
// ----------------------------------------------------------------------------------------------------------------------------

var Default Configurator = func(config *Config) {
	config.isDefault = true
}

func IsDefault(isDefault bool) Configurator {
	return func(config *Config) {
		config.isDefault = isDefault
	}
}

// ----------------------------------------------------------------------------------------------------------------------------
// Duplicate (unnamed)
// ----------------------------------------------------------------------------------------------------------------------------

var Duplicate Configurator = func(config *Config) {
	config.duplicate = true
}
