package unit_tests

import (
	"gitlab.com/atrico/container/v2"
	. "gitlab.com/atrico/testing/v2"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"testing"
)

func Test_ResolveMap(t *testing.T) {
	for _, testCase := range mapSliceRegisterTestCases[map[string]TheType](4) {
		t.Run(testCase.name, func(t *testing.T) {
			func(testCase mapSliceRegisterTestCase[map[string]TheType]) {
				// Arrange
				const name1 = "name1"
				const name2 = "name2"
				const name3 = "name3"
				const name4 = "name4"
				c := container.NewContainer().
					Register(func() TheType { return TheType(name1) }, append(testCase.configurators[0], container.Name(name1))...).
					Register(func() TheType { return TheType(name2) }, append(testCase.configurators[1], container.Name(name2))...).
					Register(func() TheType { return TheType(name3) }, append(testCase.configurators[2], container.Name(name3))...).
					Register(func() TheType { return TheType(name4) }, append(testCase.configurators[3], container.Name(name4))...)

				// Act
				result := testCase.resolver(c)

				// Assert
				expected := map[string]TheType{
					name1: TheType(name1),
					name2: TheType(name2),
					name3: TheType(name3),
					name4: TheType(name4),
				}
				assert.That(t, result, is.EqualTo(expected), "map")
			}(testCase)
		})
	}
}

func Test_ResolveMapPlusDefault(t *testing.T) {
	for _, testCase := range mapSliceRegisterTestCases[map[string]TheType](4) {
		t.Run(testCase.name, func(t *testing.T) {
			func(testCase mapSliceRegisterTestCase[map[string]TheType]) {
				// Arrange
				const name1 = "name1"
				const name2 = "name2"
				const name3 = "name3"
				const name4 = ""
				c := container.NewContainer().
					Register(func() TheType { return TheType(name1) }, append(testCase.configurators[0], container.Name(name1))...).
					Register(func() TheType { return TheType(name2) }, append(testCase.configurators[1], container.Name(name2))...).
					Register(func() TheType { return TheType(name3) }, append(testCase.configurators[2], container.Name(name3))...).
					Register(func() TheType { return TheType(name4) }, append(testCase.configurators[3], container.Name(name4))...)

				// Act
				result := testCase.resolver(c)

				// Assert
				expected := map[string]TheType{
					name1: TheType(name1),
					name2: TheType(name2),
					name3: TheType(name3),
					name4: TheType(name4),
				}
				assert.That(t, result, is.EqualTo(expected), "map")
			}(testCase)
		})
	}
}

func Test_ResolveMapOverride(t *testing.T) {
	for _, testCase := range mapSliceRegisterTestCases[map[string]TheType](5) {
		t.Run(testCase.name, func(t *testing.T) {
			func(testCase mapSliceRegisterTestCase[map[string]TheType]) {
				expected := map[string]TheType{
					"one": TheType("one"),
					"two": TheType("two"),
				}
				// Arrange
				const name1 = "name1"
				const name2 = "name2"
				const name3 = "name3"
				const name4 = "name4"
				c := container.NewContainer().
					Register(func() TheType { return TheType(name1) }, append(testCase.configurators[0], container.Name(name1))...).
					Register(func() TheType { return TheType(name2) }, append(testCase.configurators[1], container.Name(name2))...).
					Register(func() TheType { return TheType(name3) }, append(testCase.configurators[2], container.Name(name3))...).
					Register(func() TheType { return TheType(name4) }, append(testCase.configurators[3], container.Name(name4))...).
					Register(func() map[string]TheType { return expected }, testCase.configurators[4]...)

				// Act
				result := testCase.resolver(c)

				// Assert
				assert.That(t, result, is.EqualTo(expected), "map")
			}(testCase)
		})
	}
}

func Test_ResolveMapNoNames(t *testing.T) {
	for _, testCase := range mapSliceRegisterTestCases[map[string]TheType](1) {
		t.Run(testCase.name, func(t *testing.T) {
			func(testCase mapSliceRegisterTestCase[map[string]TheType]) {
				// Arrange
				const name1 = "name1"
				c := container.NewContainer().
					Register(func() TheType { return TheType(name1) }, testCase.configurators[0]...)

				// Act
				thePanic := PanicCatcher(func() {
					testCase.resolver(c)
				})

				// Assert
				const expectedPanic = "no concrete found for the abstraction: map[string]unit_tests.TheType"
				assert.That(t, thePanic.(string), is.EqualTo(expectedPanic), "Expected panic")
			}(testCase)
		})
	}
}

func Test_ResolveMapIgnoreDuplicates(t *testing.T) {
	for _, testCase := range mapSliceRegisterTestCases[map[string]TheType](4) {
		t.Run(testCase.name, func(t *testing.T) {
			func(testCase mapSliceRegisterTestCase[map[string]TheType]) {
				// Arrange
				const name1 = "name1"
				const name2 = "name2"
				const name3 = "name3"
				const name4 = ""
				c := container.NewContainer().
					Register(func() TheType { return TheType(name1) }, append(testCase.configurators[0], container.Duplicate)...).
					Register(func() TheType { return TheType(name2) }, append(testCase.configurators[1], container.Name(name2))...).
					Register(func() TheType { return TheType(name3) }, append(testCase.configurators[2], container.Duplicate)...).
					Register(func() TheType { return TheType(name4) }, append(testCase.configurators[3], container.Name(name4))...)

				// Act
				result := testCase.resolver(c)

				// Assert
				expected := map[string]TheType{
					name2: TheType(name2),
					name4: TheType(name4),
				}
				assert.That(t, result, is.EqualTo(expected), "map")
			}(testCase)
		})
	}
}
