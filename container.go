// Package container provides an IoC container for Go projects.
// It provides simple, fluent and easy-to-use interface to make dependency injection in GoLang easier.
package container

import (
	"fmt"
	"reflect"
	"regexp"
	"slices"
	"sort"
	"strings"
)

// Container interface for resolving registered concrete types
type Container interface {
	// Resolve will resolve the dependency and return a appropriate concrete of the given abstraction.
	// It can take an abstraction (interface reference) and fill it with the related implementation.
	// It also can take a function (receiver) with one or more arguments of the abstractions (interfaces) that need to be
	// resolved, Container will invoke the receiver function and pass the related implementations.
	Resolve(receiver any)
	// ResolveByName will resolve the named dependency and return a appropriate concrete of the given abstraction.
	// It can take an abstraction (interface reference) and fill it with the related implementation.
	// It also can take a function (receiver) with one or more arguments of the abstractions (interfaces) that need to be
	// resolved, Container will invoke the receiver function and pass the related implementations.
	ResolveByName(name string, receiver any)
	// ResolveByNameOrDefault will resolve the named dependency and return a appropriate concrete of the given abstraction.
	// If named value doesn't exist, default value will be returned
	ResolveByNameOrDefault(name string, receiver any)

	// Status of the container, used for debugging
	Status() StatusType
	// Status as above but with reg ex filter
	StatusWithFilter(filter string) StatusType
}

type StatusType map[string]map[string]BindInfo
type BindInfo struct {
	DefaultTo string
	Transient bool
	Anonymous bool
}

// Create empty container
// Add container itself
func NewContainer() ContainerBuilder {
	c := &container{}
	c.Register(func() Container { return c })
	return c
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------

// Default implementation of Container
// container is the IoC container that will keep all of the bindings.
type container map[reflect.Type]map[string]binding

func (c *container) Resolve(receiver any) {
	c.ResolveByName("", receiver)
}

func (c *container) ResolveByName(name string, receiver any) {
	c.resolveByName(name, receiver, true)
}
func (c *container) ResolveByNameOrDefault(name string, receiver any) {
	c.resolveByName(name, receiver, false)
}
func (c *container) resolveByName(name string, receiver any, strict bool) {
	receiverTypeOf := reflect.TypeOf(receiver)
	if receiverTypeOf == nil {
		panic("cannot detect type of the receiver, make sure your are passing reference of the object")
	}

	if receiverTypeOf.Kind() == reflect.Ptr {
		abstraction := receiverTypeOf.Elem()

		if instance := c.resolve(name, strict, abstraction); instance != nil {
			reflect.ValueOf(receiver).Elem().Set(reflect.ValueOf(instance))
			return
		}

		panic("no concrete found for the abstraction: " + abstraction.String())
	}

	if receiverTypeOf.Kind() == reflect.Func {
		arguments := c.arguments(name, receiver)
		reflect.ValueOf(receiver).Call(arguments)
		return
	}

	panic("the receiver must be either a reference or a callback")
}

func (c *container) Status() (status StatusType) {
	return c.status("")
}
func (c *container) StatusWithFilter(filter string) (status StatusType) {
	return c.status(filter)
}
func (c *container) status(filterS string) (status StatusType) {
	filter := regexp.MustCompile(filterS)
	status = make(map[string]map[string]BindInfo, len(*c))
	// List all types
	for t, tInfo := range *c {
		tStr := t.String()
		if filter.MatchString(tStr) {
			status[tStr] = make(map[string]BindInfo, len(tInfo))
			// Names
			for n, nBind := range tInfo {
				status[tStr][n] = nBind.status()
			}
		}
	}
	return
}

func (s StatusType) String() string {
	return strings.Join(s.StringMl(), "\n")
}

func (s StatusType) StringMl() (lines []string) {
	// Sort types
	types := make([]string, 0, len(s))
	for name, _ := range s {
		types = append(types, name)
	}
	slices.Sort(types)
	for _, typ := range types {
		lines = append(lines, typ+":")
		// Check for default only
		if info, ok := s[typ][""]; ok && len(s[typ]) == 1 {
			// Just add type line
			if info.Transient {
				lines[len(lines)-1] = fmt.Sprintf("%s: (transient)", typ)
			}
		} else {
			// Sort names
			names := make([]string, 0, len(s[typ]))
			for name, _ := range s[typ] {
				names = append(names, name)
			}
			sort.Slice(names, func(i, j int) bool {
				lhs := names[i]
				rhs := names[j]
				// Sort anonymous at the end
				if s[typ][lhs].Anonymous != s[typ][rhs].Anonymous {
					return s[typ][rhs].Anonymous
				}
				return lhs < rhs
			})
			theDefault := ""
			for _, name := range names {
				if s[typ][name].DefaultTo != "" {
					theDefault = s[typ][name].DefaultTo
				} else {
					details := strings.Builder{}
					sep := ":"
					if s[typ][name].Transient {
						details.WriteString(": (transient)")
						sep = ""
					}
					if name == theDefault {
						details.WriteString(fmt.Sprintf("%s (default)", sep))
					}
					if s[typ][name].Anonymous {
						name = "DUPLICATE"
					} else {
						name = fmt.Sprintf("'%s'", name)
					}
					lines = append(lines, fmt.Sprintf("  - %s%s", name, details.String()))
				}
			}
		}
	}
	return
}

// invoke will call the given function and return its returned value.
// It only works for functions that return a single value.
func (c *container) invoke(name string, function any) any {
	return reflect.ValueOf(function).Call(c.arguments(name, function))[0].Interface()
}

// arguments will return resolved arguments of the given function.
func (c *container) arguments(name string, function any) []reflect.Value {
	functionTypeOf := reflect.TypeOf(function)
	argumentsCount := functionTypeOf.NumIn()
	arguments := make([]reflect.Value, argumentsCount)

	for i := 0; i < argumentsCount; i++ {
		abstraction := functionTypeOf.In(i)
		instance := c.resolve(name, false, abstraction)
		if instance == nil {
			panic("no concrete found for the abstraction: " + abstraction.String())
		}
		arguments[i] = reflect.ValueOf(instance)
	}

	return arguments
}

// resolve will return the concrete of related abstraction.
func (c *container) resolve(name string, strict bool, abstraction reflect.Type) any {
	if b, ok := (*c)[abstraction][name]; ok {
		return b.resolve(c, name)
	}
	if !strict {
		if b, ok := (*c)[abstraction][""]; ok {
			return b.resolve(c, name)
		}
	}

	// Check for map of named values
	if abstraction.Kind() == reflect.Map && abstraction.Key().Kind() == reflect.String {
		if instance := c.resolveNamedObjects(abstraction.Elem()); instance != nil {
			aMap := reflect.MakeMapWithSize(reflect.MapOf(abstraction.Key(), abstraction.Elem()), 0)
			for k, v := range instance {
				aMap.SetMapIndex(reflect.ValueOf(k), reflect.ValueOf(v))
			}
			return aMap.Interface()
		}
	}

	// Check for array of values
	if abstraction.Kind() == reflect.Slice {
		instance := c.resolveAll(abstraction.Elem())
		arr := reflect.MakeSlice(reflect.SliceOf(abstraction.Elem()), 0, len(instance))
		for _, v := range instance {
			arr = reflect.Append(arr, reflect.ValueOf(v))
		}
		return arr.Interface()
	}

	return nil
}

func (c *container) resolveNamedObjects(abstraction reflect.Type) (result map[string]any) {
	// Find all of this type
	if all, ok := (*c)[abstraction]; ok {
		for name, b := range all {
			if !b.isDefault() && !b.isAnonymous() {
				if result == nil {
					result = make(map[string]any)
				}
				result[name] = b.resolve(c, name)
			}
		}
		// Are there named values?
		if len(result) == 1 {
			if _, ok = result[""]; ok {
				// Only 1 entry which is un-named
				result = nil
			}
		}
	}
	return
}

func (c *container) resolveAll(abstraction reflect.Type) (result []any) {
	// Find all of this type
	if all, ok := (*c)[abstraction]; ok {
		for name, b := range all {
			if !b.isDefault() {
				result = append(result, b.resolve(c, name))
			}
		}
	}
	return
}
