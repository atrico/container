package unit_tests

import (
	"testing"

	. "gitlab.com/atrico/testing/v2"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"

	"gitlab.com/atrico/container/v2"
)

type Shape interface {
	SetArea(int)
	GetArea() int
}

type Circle struct {
	a int
}

func (c *Circle) SetArea(a int) {
	c.a = a
}

func (c Circle) GetArea() int {
	return c.a
}

type Database interface {
	Connect() bool
}

type MySQL struct{}

func (m MySQL) Connect() bool {
	return true
}

func TestSingletonItShouldMakeAnInstanceOfTheAbstraction(t *testing.T) {
	area := 5

	c := container.NewContainer().
		Register(func() Shape {
			return &Circle{a: area}
		})

	c.Resolve(func(s Shape) {
		a := s.GetArea()
		assert.That(t, a, is.EqualTo(area), "")
	})
}

func TestSingletonItShouldMakeSameObjectEachMake(t *testing.T) {
	c := container.NewContainer().
		Register(func() Shape {
			return &Circle{a: 5}
		})

	area := 6

	c.Resolve(func(s1 Shape) {
		s1.SetArea(area)
	})

	c.Resolve(func(s2 Shape) {
		a := s2.GetArea()
		assert.That(t, a, is.EqualTo(area), "")
	})
}

func TestSingletonWithNonFunctionResolverItShouldPanic(t *testing.T) {
	value := "the resolver must be a function"
	c := container.NewContainer()
	thePanic := PanicCatcher(func() {
		c.Register("STRING!")
	})
	assert.That(t, thePanic.(string), is.EqualTo(value), "Expected panic")
}

func TestSingletonItShouldResolveResolverArguments(t *testing.T) {
	area := 5
	c := container.NewContainer()
	c.Register(func() Shape {
		return &Circle{a: area}
	})

	c.Register(func(s Shape) Database {
		assert.That(t, s.GetArea(), is.EqualTo(area), "")
		return &MySQL{}
	})
}

func TestTransientItShouldMakeDifferentObjectsOnMake(t *testing.T) {
	area := 5

	c := container.NewContainer().
		Register(func() Shape {
			return &Circle{a: area}
		}, container.Transient)

	c.Resolve(func(s1 Shape) {
		s1.SetArea(6)
	})

	c.Resolve(func(s2 Shape) {
		a := s2.GetArea()
		assert.That(t, a, is.EqualTo(area), "")
	})
}

func TestTransientItShouldMakeAnInstanceOfTheAbstraction(t *testing.T) {
	area := 5

	c := container.NewContainer().
		Register(func() Shape {
			return &Circle{a: area}
		}, container.Transient)

	c.Resolve(func(s Shape) {
		a := s.GetArea()
		assert.That(t, a, is.EqualTo(area), "")
	})
}

func TestMakeWithSingleInputAndCallback(t *testing.T) {
	c := container.NewContainer().
		Register(func() Shape {
			return &Circle{a: 5}
		})

	c.Resolve(func(s Shape) {
		if _, ok := s.(*Circle); !ok {
			t.Error("Expected Circle")
		}
	})
}

func TestMakeWithMultipleInputsAndCallback(t *testing.T) {
	c := container.NewContainer().
		Register(func() Shape {
			return &Circle{a: 5}
		}).
		Register(func() Database {
			return &MySQL{}
		})

	c.Resolve(func(s Shape, m Database) {
		if _, ok := s.(*Circle); !ok {
			t.Error("Expected Circle")
		}

		if _, ok := m.(*MySQL); !ok {
			t.Error("Expected MySQL")
		}
	})
}

func TestMakeWithSingleInputAndReference(t *testing.T) {
	c := container.
		NewContainer().Register(func() Shape {
		return &Circle{a: 5}
	})

	var s Shape

	c.Resolve(&s)

	if _, ok := s.(*Circle); !ok {
		t.Error("Expected Circle")
	}
}

func TestMakeWithMultipleInputsAndReference(t *testing.T) {
	c := container.NewContainer().
		Register(func() Shape {
			return &Circle{a: 5}
		}).
		Register(func() Database {
			return &MySQL{}
		})

	var (
		s Shape
		d Database
	)

	c.Resolve(&s)
	c.Resolve(&d)

	if _, ok := s.(*Circle); !ok {
		t.Error("Expected Circle")
	}

	if _, ok := d.(*MySQL); !ok {
		t.Error("Expected MySQL")
	}
}

func TestMakeWithUnsupportedReceiver(t *testing.T) {
	value := "the receiver must be either a reference or a callback"
	c := container.NewContainer()
	thePanic := PanicCatcher(func() {
		c.Resolve("STRING!")
	})
	assert.That(t, thePanic.(string), is.EqualTo(value), "Expected panic")
}

func TestMakeWithNonReference(t *testing.T) {
	value := "cannot detect type of the receiver, make sure your are passing reference of the object"
	c := container.NewContainer()
	thePanic := PanicCatcher(func() {
		var s Shape
		c.Resolve(s)
	})
	assert.That(t, thePanic.(string), is.EqualTo(value), "Expected panic")
}

func TestMakeWithUnboundedAbstraction(t *testing.T) {
	value := "no concrete found for the abstraction: unit_tests.Shape"
	c := container.NewContainer()
	thePanic := PanicCatcher(func() {
		var s Shape
		c.Clear()
		c.Resolve(&s)
	})
	assert.That(t, thePanic.(string), is.EqualTo(value), "Expected panic")
}

func TestMakeWithCallbackThatHasAUnboundedAbstraction(t *testing.T) {
	value := "no concrete found for the abstraction: unit_tests.Database"
	c := container.NewContainer()
	thePanic := PanicCatcher(func() {
		c.Clear()
		c.Register(func() Shape {
			return &Circle{}
		})
		c.Resolve(func(s Shape, d Database) {})
	})
	assert.That(t, thePanic.(string), is.EqualTo(value), "Expected panic")
}
