package unit_tests

import (
	"fmt"
	"gitlab.com/atrico/container/v2"
	. "gitlab.com/atrico/testing/v2"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"testing"
)

// ----------------------------------------------------------------------------------------------------------------------------
// Default resolve
// ----------------------------------------------------------------------------------------------------------------------------

func Test_ResolveDefault(t *testing.T) {
	for _, testCase := range namedRegisterTestCases(2) {
		t.Run(testCase.name, func(t *testing.T) {
			func(testCase namedRegisterTestCase) {
				// Arrange
				const defaultValue = TheType("default")
				const value1 = TheType("value1")
				c := container.NewContainer().
					Register(func() TheType { return defaultValue }, testCase.configurators[0]...).
					Register(func() TheType { return value1 }, append(testCase.configurators[1], container.Name("name"))...)

				// Act
				var result TheType
				c.Resolve(&result)

				// Assert
				assert.That(t, result, is.EqualTo(defaultValue), "Should return default")
			}(testCase)
		})
	}
}

func Test_ResolveDefaultExplicit(t *testing.T) {
	for _, testCase := range namedRegisterTestCases(2) {
		t.Run(testCase.name, func(t *testing.T) {
			func(testCase namedRegisterTestCase) {
				// Arrange
				const defaultValue = TheType("default")
				c := container.NewContainer().
					Register(func() TheType { return defaultValue }, append(testCase.configurators[0], container.Name(""))...).
					Register(func() TheType { return TheType("name") }, append(testCase.configurators[1], container.Name("name"))...)

				// Act
				var result TheType
				c.Resolve(&result)

				// Assert
				assert.That(t, result, is.EqualTo(defaultValue), "Should return default")
			}(testCase)
		})
	}
}

func Test_ResolveImpliedDefault(t *testing.T) {
	for _, testCase := range namedRegisterTestCases(2) {
		t.Run(testCase.name, func(t *testing.T) {
			func(testCase namedRegisterTestCase) {
				// Arrange
				const value1 = TheType("value1")
				const value2 = TheType("value2")
				c := container.NewContainer().
					Register(func() TheType { return value1 }, append(testCase.configurators[0], container.Name("name1"))...).
					Register(func() TheType { return value2 }, append(testCase.configurators[1], container.Name("nam2e"))...)

				// Act
				var result TheType
				c.Resolve(&result)

				// Assert
				assert.That(t, result, is.EqualTo(value1), "Should return default")
			}(testCase)
		})
	}
}

func Test_ResolveConfiguredDefault(t *testing.T) {
	for _, testCase := range namedRegisterTestCases(2) {
		t.Run(testCase.name, func(t *testing.T) {
			func(testCase namedRegisterTestCase) {
				// Arrange
				const value1 = TheType("value1")
				const value2 = TheType("value2")
				c := container.NewContainer().
					Register(func() TheType { return value1 }, append(testCase.configurators[0], container.Name("name1"))...).
					Register(func() TheType { return value2 }, append(testCase.configurators[1], container.Name("nam2e"), container.Default)...)

				// Act
				var result TheType
				c.Resolve(&result)

				// Assert
				assert.That(t, result, is.EqualTo(value2), "Should return default")
			}(testCase)
		})
	}
}

// ----------------------------------------------------------------------------------------------------------------------------
// Resolve by name
// ----------------------------------------------------------------------------------------------------------------------------

func Test_ResolveByName(t *testing.T) {
	for _, testCase := range namedRegisterTestCases(2) {
		t.Run(testCase.name, func(t *testing.T) {
			func(testCase namedRegisterTestCase) {
				// Arrange
				const name1 = "name1"
				const value1 = TheType("value1")
				const name2 = "name2"
				const value2 = TheType("value2")
				c := container.NewContainer().
					Register(func() TheType { return value1 }, append(testCase.configurators[0], container.Name(name1))...).
					Register(func() TheType { return value2 }, append(testCase.configurators[1], container.Name(name2))...)

				// Act
				var result1 TheType
				c.ResolveByName(name1, &result1)
				var result2 TheType
				c.ResolveByName(name2, &result2)

				// Assert
				assert.That(t, result1, is.EqualTo(value1), "value 1")
				assert.That(t, result2, is.EqualTo(value2), "value 2")
			}(testCase)
		})
	}
}

func Test_ResolveByNameMissing(t *testing.T) {
	for _, testCase := range namedRegisterTestCases(2) {
		t.Run(testCase.name, func(t *testing.T) {
			func(testCase namedRegisterTestCase) {
				// Arrange
				const name1 = "name1"
				const value1 = TheType("value1")
				const name2 = "name2"
				c := container.NewContainer().
					Register(func() TheType { return value1 }, append(testCase.configurators[1], container.Name(name1))...)

				// Act
				thePanic := PanicCatcher(func() {
					var result TheType
					c.ResolveByName(name2, &result)
				})

				// Assert
				const expectedPanic = "no concrete found for the abstraction: unit_tests.TheType"
				assert.That(t, thePanic.(string), is.EqualTo(expectedPanic), "Expected panic")
			}(testCase)
		})
	}
}

func Test_ResolveByNameOrDefaultMissing(t *testing.T) {
	for _, testCase := range namedRegisterTestCases(2) {
		t.Run(testCase.name, func(t *testing.T) {
			func(testCase namedRegisterTestCase) {
				// Arrange
				const name1 = "name1"
				const value1 = TheType("value1")
				const name2 = "name2"
				c := container.NewContainer().
					Register(func() TheType { return value1 }, append(testCase.configurators[1], container.Name(name1))...)

				var result TheType
				c.ResolveByNameOrDefault(name2, &result)

				// Assert
				assert.That(t, result, is.EqualTo(value1), "default value")
			}(testCase)
		})
	}
}

type TheType2 string

func Test_ResolveByNameDependencies(t *testing.T) {
	for _, testCase := range namedRegisterTestCases(4) {
		t.Run(testCase.name, func(t *testing.T) {
			func(testCase namedRegisterTestCase) {
				// Arrange
				const name1 = "name1"
				const name2 = "name2"
				const value1 = "value1"
				const value2 = "value2"
				c := container.NewContainer().
					Register(func(dep TheType2) TheType { return TheType(fmt.Sprintf("%s - %s", dep, value1)) }, append(testCase.configurators[0], container.Name(name1))...).
					Register(func() TheType2 { return TheType2(value1) }, append(testCase.configurators[1], container.Name(name1))...).
					Register(func(dep TheType2) TheType { return TheType(fmt.Sprintf("%s - %s", dep, value2)) }, append(testCase.configurators[2], container.Name(name2))...).
					Register(func() TheType2 { return TheType2(value2) }, testCase.configurators[3]...)

				// Act
				var result1 TheType
				c.ResolveByName(name1, &result1)
				var result2 TheType
				c.ResolveByName(name2, &result2)

				// Assert
				assert.That(t, string(result1), is.EqualTo(fmt.Sprintf("%s - %s", value1, value1)), "value 1")
				assert.That(t, string(result2), is.EqualTo(fmt.Sprintf("%s - %s", value2, value2)), "value 2")
			}(testCase)
		})
	}
}
